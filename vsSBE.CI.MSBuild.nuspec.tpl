<?xml version="1.0"?>
<package >
  <metadata>
    <id>vsSBE.CI.MSBuild</id>
    <title>vsSolutionBuildEvent CI.MSBuild</title>
    <version>%CIMVersion%%PackageVersion%</version>
    <authors>entry.reg@gmail.com</authors>
    <owners>reg</owners>
    <licenseUrl>https://visualstudiogallery.msdn.microsoft.com/0d1dbfd7-ed8a-40af-ae39-281bfeca2334/</licenseUrl>
    <projectUrl>https://visualstudiogallery.msdn.microsoft.com/0d1dbfd7-ed8a-40af-ae39-281bfeca2334/</projectUrl>
    <iconUrl>https://bitbucket.org/3F/vssolutionbuildevent/raw/master/vsSolutionBuildEvent/Resources/Package.png</iconUrl>
    <requireLicenseAcceptance>false</requireLicenseAcceptance>
    <description>Official package of libraries to support the CI /Build Servers for plugin vsSolutionBuildEvent.
    
    *!* See our documentation of How to use it !
    
    * Documentation: http://vssbe.r-eg.net -> http://vssbe.r-eg.net/doc/CI/CI.MSBuild/
    * VS Gallery Page: https://visualstudiogallery.msdn.microsoft.com/0d1dbfd7-ed8a-40af-ae39-281bfeca2334/
    * Sources &amp; Public Issue Tracker: 
        * https://github.com/3F/vsSolutionBuildEvent
        * https://bitbucket.org/3F/vssolutionbuildevent
    
    </description>
    <releaseNotes>Updated the vsSolutionBuildEvent library: v%vsSBEVersion%</releaseNotes>
    <copyright>Copyright (c) 2013-2015  Denis Kuzmin (reg) [entry.reg@gmail.com]</copyright>
    <tags>CI Tools vsSolutionBuildEvent MSBuild logging automation vsSBE Scripts Build Events Solution Projects Versioning AppVeyor TeamCity Build-Server Continuous-Integration Build-automation</tags>
  </metadata>
</package>